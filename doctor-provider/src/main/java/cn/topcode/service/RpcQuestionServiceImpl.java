package cn.topcode.service;

import cn.topcode.beans.pojo.Image;
import cn.topcode.beans.pojo.Question;
import cn.topcode.beans.pojo.User;
import cn.topcode.beans.vo.questionVO.FreeQuestionVO;
import cn.topcode.dao.ImageMapper;
import cn.topcode.dao.QuestionMapper;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

@Component
@Service(interfaceClass = RpcQuestionService.class)
public class RpcQuestionServiceImpl implements RpcQuestionService {

    @Resource
    private QuestionMapper questionMapper;
    @Resource
    private ImageMapper imageMapper;

    /***
     * 增加提问记录
     * @param freeQuestionVO
     * @return
     * @throws Exception
     */
    @Override
    public boolean addQuestion(FreeQuestionVO freeQuestionVO, User currentUser) throws Exception {
        if (null != freeQuestionVO && null != currentUser) {
            Question question = new Question();
            question.setUId(currentUser.getUId());
            question.setUName(currentUser.getNickname());
            question.setQContent(freeQuestionVO.getqContext());
            question.setCreateDate(new Date());
            Integer qId = 0;
            if (questionMapper.insertQuestion(question) > 0) {
                if (null != freeQuestionVO.getImageURL()) {
                    qId = question.getQId();
                    Image image = null;
                    for (String url : freeQuestionVO.getImageURL()) {
                        image = new Image();
                        image.setTargetId(qId);
                        image.setPath(url);
                        image.setType(0);
                        image.setCreateDate(new Date());
                        imageMapper.insertImage(image);
                    }
                }
                return true;
            }
        }
        return false;
    }
}
