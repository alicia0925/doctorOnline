package cn.topcode.service;

import cn.topcode.beans.vo.articleVO.ArticleListVO;
import cn.topcode.dao.ArticleMapper;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component
@Service(interfaceClass = RpcArticleService.class)
public class RpcArticleServiceImpl implements RpcArticleService {

    @Resource
    private ArticleMapper articleMapper;

    /***
     * 根据条件查询所有文章
     * @param map 查询条件
     * @return
     * @throws Exception
     */
    @Override
    public List<ArticleListVO> findArticleByQuery(Map map) throws Exception {
        return articleMapper.selectArticleByQuery(map);
    }

    /***
     * 修改阅读量
     * @param articleId 文章id
     * @param count 阅读量
     * @return
     * @throws Exception
     */
    @Override
    public boolean updateCount(Integer articleId, Integer count) throws Exception {
        if (articleMapper.updateCountByArticleId(articleId, count + 1) > 0)
            return true;
        return false;
    }

    /***
     * 修改收藏量
     * @param articleId 文章id
     * @param likes 收藏量
     * @return
     * @throws Exception
     */
    @Override
    public boolean updateLikes(Integer articleId, Integer likes) throws Exception {
        if (articleMapper.updateLikesByArticleId(articleId, likes) > 0)
            return true;
        return false;
    }
}
