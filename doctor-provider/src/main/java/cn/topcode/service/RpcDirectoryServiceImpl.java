package cn.topcode.service;

import cn.topcode.beans.vo.articleVO.ArticleIndexVO;
import cn.topcode.dao.DirectoryMapper;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Service(interfaceClass = RpcDirectoryService.class)
public class RpcDirectoryServiceImpl implements RpcDirectoryService {

    @Resource
    private DirectoryMapper directoryMapper;

    /***
     * 查询数据字典中所有文章级别
     * @return
     * @throws Exception
     */
    @Override
    public List<ArticleIndexVO> findArticleTag() throws Exception {
        return directoryMapper.selectArticleTag();
    }
}
