package cn.topcode.service;

import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.DoctorSeacherVo;
import cn.topcode.dao.DoctorMapper;
import cn.topcode.utils.EmptyUtils;

import javax.annotation.Resource;
import java.util.List;

public class RpcDoctorServiceImpl implements RpcDoctorService {
    @Resource
    private DoctorMapper doctorMapper;

    @Override
    public List<Doctor> findDoctorsByConditions(DoctorSeacherVo doctorSeacherVo) throws Exception {
        if(EmptyUtils.isNotEmpty(doctorSeacherVo)){
            return doctorMapper.getList(doctorSeacherVo);
        }
        return null;
    }
}
