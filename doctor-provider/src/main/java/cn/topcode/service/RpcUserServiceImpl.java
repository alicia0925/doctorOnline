package cn.topcode.service;

import cn.topcode.beans.pojo.User;
import cn.topcode.beans.vo.userVO.UserRegisterVO;
import cn.topcode.dao.UserMapper;
import cn.topcode.utils.EmptyUtils;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;

@Component
@Service(interfaceClass = RpcUserService.class)
public class RpcUserServiceImpl implements RpcUserService {

    @Resource
    private UserMapper userMapper;



    /***
     * 用户手机登录
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    @Override
    public User login (String phone,String password) throws Exception {
        if(EmptyUtils.isNotEmpty(phone)&&EmptyUtils.isNotEmpty(password)){
            User user=userMapper.selectByPhone(phone);
          if(EmptyUtils.isNotEmpty(user)){
              if(user.getPassword().equals(password)){
                  return user;
              }

          }
        }
        return null;
    }
    /***
     * 验证用户是否存在
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    @Override
    public boolean validateUser(String phone) throws Exception {
        if (null == userMapper.selectByPhone(phone))
            return false;
        return true;
    }

    /***
     * 注册用户
     * @param userRegisterVO 注册参数
     * @return
     * @throws Exception
     */
    @Override
    public boolean registerUser(UserRegisterVO userRegisterVO) throws Exception {
        if (userMapper.insertUser(userRegisterVO) > 0)
            return true;
        return false;
    }
}
