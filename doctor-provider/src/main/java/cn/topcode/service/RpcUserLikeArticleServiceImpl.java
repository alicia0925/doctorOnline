package cn.topcode.service;

import cn.topcode.beans.pojo.UserLikeArticle;
import cn.topcode.dao.UserLikeArticleMapper;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component
@Service(interfaceClass = RpcUserLikeArticleService.class)
public class RpcUserLikeArticleServiceImpl implements RpcUserLikeArticleService {

    @Resource
    private UserLikeArticleMapper userLikeArticleMapper;

    /***
     * 根据条件查询用户收藏记录
     * @param map 查询条件
     * @return
     * @throws Exception
     */
    @Override
    public UserLikeArticle findUserLikeArticleByQuery(Map map) throws Exception {
        return userLikeArticleMapper.selectUserLikeArticleByQuery(map);
    }

    /***
     * 添加用户收藏记录
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    @Override
    public boolean addUserLikeArticle(UserLikeArticle userLikeArticle) throws Exception {
        if (userLikeArticleMapper.insertUserLikeArticle(userLikeArticle) > 0)
            return true;
        return false;
    }

    /***
     * 修改收藏状态
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    @Override
    public boolean updateUserLikeArticle(UserLikeArticle userLikeArticle) throws Exception {
        if (userLikeArticle.getStatus() == 0) {
            userLikeArticle.setStatus(1);
        } else {
            userLikeArticle.setStatus(0);
        }
        if (userLikeArticleMapper.updateUserLikeArticleByQuery(userLikeArticle) > 0)
            return true;
        return false;
    }
}
