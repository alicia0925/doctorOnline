package cn.topcode.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by Administrator on 2018/9/7.
 */
@Configuration //<beans>
@Component
public class RedisConfig {
    /**
     * <bean id="redisTemplate" class="RedisTemplate全路径">
     *     <propertity name="connectionFactory" ref="redisConnectionFactory"></propertity>
     * <beans/>
     * @param redisConnectionFactory
     * @return
     */

    @Bean
//    @Scope("")
    public RedisTemplate getRedisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate redisTemplate=new RedisTemplate();
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        return redisTemplate;
    }

    @Bean
//    @Scope("")
    public StringRedisTemplate getStringRedisTemplate(RedisConnectionFactory redisConnectionFactory){
        StringRedisTemplate stringRedisTemplate=new StringRedisTemplate();
        stringRedisTemplate.setConnectionFactory(redisConnectionFactory);
        return stringRedisTemplate;
    }


    @Bean
    public RedisConnectionFactory redisConnectionFactory(){
        RedisConnectionFactory redisConnectionFactory= new JedisConnectionFactory();
        return redisConnectionFactory;
    }

    @Bean
    public JedisPool redisPoolFactory(){
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisPool jedisPool = new JedisPool();
        return jedisPool;
    }
}
