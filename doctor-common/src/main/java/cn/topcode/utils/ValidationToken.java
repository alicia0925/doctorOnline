package cn.topcode.utils;

import cn.topcode.beans.pojo.User;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ValidationToken {

    @Resource
    private RedisAPI redisAPI;

    public User getCurrentUser(String tokenString) {
        //根据token从redis中获取用户信息
        User user = null;
        if (null == tokenString || "".equals(tokenString)) {
            return null;
        }
        try {
            String userInfoJson = redisAPI.get(tokenString);
            user = JSONObject.parseObject(userInfoJson, User.class);
        } catch (Exception e) {
            user = null;
        }
        return user;
    }
}
