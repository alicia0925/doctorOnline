package cn.topcode.beans.vo.doctor;


import java.io.Serializable;

public class DoctorSeacherVo implements Serializable {
    private Integer  depId; //科室ID
    private String desc;//降序
    private String asc; //升序
    private Integer  serviceId;


    public DoctorSeacherVo(Integer depId, String desc, String asc, Integer serviceId) {
        this.depId = depId;
        this.desc = desc;
        this.asc = asc;
        this.serviceId = serviceId;
    }

    public DoctorSeacherVo() {
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAsc() {
        return asc;
    }

    public void setAsc(String asc) {
        this.asc = asc;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }
}
