package cn.topcode.beans.vo.user;

public class TokenVo {
    private String token;
    /**
     * 过期时间,单位：毫秒
     */
    //"过期时间，单位：毫秒"
    private long expTime;
    /**
     * 生成时间,单位：毫秒
     */
    private long genTime;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getExpTime() {
        return expTime;
    }

    public void setExpTime(long expTime) {
        this.expTime = expTime;
    }

    public long getGenTime() {
        return genTime;
    }

    public void setGenTime(long genTime) {
        this.genTime = genTime;
    }

    public TokenVo() {
        super();
    }

    public TokenVo(String token, long expTime, long genTime) {
        super();
        this.token = token;
        this.expTime = expTime;
        this.genTime = genTime;
    }
}
