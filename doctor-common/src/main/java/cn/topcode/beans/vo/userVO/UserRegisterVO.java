package cn.topcode.beans.vo.userVO;

import java.io.Serializable;
import java.util.Date;

public class UserRegisterVO implements Serializable {
    private String phone;//手机号
    private String password;//密码
    private String code;//注册码
    private String nickname;//昵称
    private Date createDate;//注册时间

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
