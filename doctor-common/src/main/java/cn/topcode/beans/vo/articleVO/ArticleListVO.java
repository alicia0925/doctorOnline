package cn.topcode.beans.vo.articleVO;

import java.io.Serializable;
import java.util.Date;

public class ArticleListVO implements Serializable {
    private Integer articleId;//文章id
    private Integer docId;//医生id
    private Integer count;//阅读量
    private Integer likes;//收藏量
    private String articleTittle;//文章标题
    private String summary;//摘要
    private String content;//文章内容
    private Integer tagFirst;//一级分类
    private Integer tagSecond;//二级分类
    private Integer tagThree;//三级分类
    private Date createDate;//发布时间

    private String docName;//医生姓名
    private String hospital;//医院名称
    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getArticleTittle() {
        return articleTittle;
    }

    public void setArticleTittle(String articleTittle) {
        this.articleTittle = articleTittle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getTagFirst() {
        return tagFirst;
    }

    public void setTagFirst(Integer tagFirst) {
        this.tagFirst = tagFirst;
    }

    public Integer getTagSecond() {
        return tagSecond;
    }

    public void setTagSecond(Integer tagSecond) {
        this.tagSecond = tagSecond;
    }

    public Integer getTagThree() {
        return tagThree;
    }

    public void setTagThree(Integer tagThree) {
        this.tagThree = tagThree;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
}
