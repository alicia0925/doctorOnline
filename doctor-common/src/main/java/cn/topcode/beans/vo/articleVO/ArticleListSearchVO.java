package cn.topcode.beans.vo.articleVO;

import java.io.Serializable;

public class ArticleListSearchVO implements Serializable {
    private Integer tagFirst;//一级分类
    private Integer tagSecond;//二级分类
    private Integer tagThree;//三级分类

    public Integer getTagFirst() {
        return tagFirst;
    }

    public void setTagFirst(Integer tagFirst) {
        this.tagFirst = tagFirst;
    }

    public Integer getTagSecond() {
        return tagSecond;
    }

    public void setTagSecond(Integer tagSecond) {
        this.tagSecond = tagSecond;
    }

    public Integer getTagThree() {
        return tagThree;
    }

    public void setTagThree(Integer tagThree) {
        this.tagThree = tagThree;
    }
}
