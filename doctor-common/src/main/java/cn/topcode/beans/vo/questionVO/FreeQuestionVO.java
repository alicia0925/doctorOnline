package cn.topcode.beans.vo.questionVO;

import cn.topcode.beans.pojo.Image;

import java.io.Serializable;

public class FreeQuestionVO implements Serializable {
    private String qContext;//问题内容
    private String[] imageURL;//图片路径

    public String getqContext() {
        return qContext;
    }

    public void setqContext(String qContext) {
        this.qContext = qContext;
    }

    public String[] getImageURL() {
        return imageURL;
    }

    public void setImageURL(String[] imageURL) {
        this.imageURL = imageURL;
    }
}
