package cn.topcode.beans.vo.articleVO;

import java.io.Serializable;

public class ArticleIndexVO implements Serializable {
    private String tagFirstName;//一级分类名称
    private String tagSecondName;//二级分类名称
    private String tagThreeName;//三级分类名称
    private Integer tagFirst;//一级分类
    private Integer tagSecond;//二级分类
    private Integer tagThree;//三级分类

    public String getTagFirstName() {
        return tagFirstName;
    }

    public void setTagFirstName(String tagFirstName) {
        this.tagFirstName = tagFirstName;
    }

    public String getTagSecondName() {
        return tagSecondName;
    }

    public void setTagSecondName(String tagSecondName) {
        this.tagSecondName = tagSecondName;
    }

    public String getTagThreeName() {
        return tagThreeName;
    }

    public void setTagThreeName(String tagThreeName) {
        this.tagThreeName = tagThreeName;
    }

    public Integer getTagFirst() {
        return tagFirst;
    }

    public void setTagFirst(Integer tagFirst) {
        this.tagFirst = tagFirst;
    }

    public Integer getTagSecond() {
        return tagSecond;
    }

    public void setTagSecond(Integer tagSecond) {
        this.tagSecond = tagSecond;
    }

    public Integer getTagThree() {
        return tagThree;
    }

    public void setTagThree(Integer tagThree) {
        this.tagThree = tagThree;
    }
}
