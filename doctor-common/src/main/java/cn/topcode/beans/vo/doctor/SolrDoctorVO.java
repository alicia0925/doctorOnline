package cn.topcode.beans.vo.doctor;

import java.io.Serializable;

public class SolrDoctorVO implements Serializable {

    private Integer  depId; //科室ID
    private String descSort;//降序
    private String ascSort; //升序

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public String getDescSort() {
        return descSort;
    }

    public void setDescSort(String descSort) {
        this.descSort = descSort;
    }

    public String getAscSort() {
        return ascSort;
    }

    public void setAscSort(String ascSort) {
        this.ascSort = ascSort;
    }
}
