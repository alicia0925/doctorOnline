package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class UserLikeArticle implements Serializable {

    private Integer uId;
    private Integer articleId;
    private Integer status;
    private Date createDate;


    public long getUId() {
        return uId;
    }

    public void setUId(Integer uId) {
        this.uId = uId;
    }


    public long getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

}
