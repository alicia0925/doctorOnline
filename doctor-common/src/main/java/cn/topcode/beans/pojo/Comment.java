package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

  private Integer commentId;
  private Integer docId;
  private Integer uId;
  private String content;
  private Date createDate;
  private Integer satisfied;
  private Integer isReference;


  public Integer getCommentId() {
    return commentId;
  }

  public void setCommentId(Integer commentId) {
    this.commentId = commentId;
  }


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }


  public Integer getUId() {
    return uId;
  }

  public void setUId(Integer uId) {
    this.uId = uId;
  }


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }


  public Integer getSatisfied() {
    return satisfied;
  }

  public void setSatisfied(Integer satisfied) {
    this.satisfied = satisfied;
  }


  public Integer getIsReference() {
    return isReference;
  }

  public void setIsReference(Integer isReference) {
    this.isReference = isReference;
  }

}
