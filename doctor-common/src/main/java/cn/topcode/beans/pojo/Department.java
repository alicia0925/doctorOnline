package cn.topcode.beans.pojo;


import java.io.Serializable;

public class Department implements Serializable {

  private Integer depId;
  private Integer depPid;
  private String depName;
  private String depInfo;


  public Integer getDepId() {
    return depId;
  }

  public void setDepId(Integer depId) {
    this.depId = depId;
  }


  public Integer getDepPid() {
    return depPid;
  }

  public void setDepPid(Integer depPid) {
    this.depPid = depPid;
  }


  public String getDepName() {
    return depName;
  }

  public void setDepName(String depName) {
    this.depName = depName;
  }


  public String getDepInfo() {
    return depInfo;
  }

  public void setDepInfo(String depInfo) {
    this.depInfo = depInfo;
  }

}
