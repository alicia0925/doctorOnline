package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class UserLikeDoc implements Serializable {

  private Integer uId;
  private Integer docId;
  private Date createDate;


  public Integer getUId() {
    return uId;
  }

  public void setUId(Integer uId) {
    this.uId = uId;
  }


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

}
