package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Question  implements Serializable {

  private Integer qId;
  private Integer uId;
  private String uName;
  private String qContent;
  private Date createDate;


  public Integer getQId() {
    return qId;
  }

  public void setQId(Integer qId) {
    this.qId = qId;
  }


  public Integer getUId() {
    return uId;
  }

  public void setUId(Integer uId) {
    this.uId = uId;
  }


  public String getUName() {
    return uName;
  }

  public void setUName(String uName) {
    this.uName = uName;
  }


  public String getQContent() {
    return qContent;
  }

  public void setQContent(String qContent) {
    this.qContent = qContent;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

}
