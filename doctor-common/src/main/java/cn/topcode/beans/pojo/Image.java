package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Image implements Serializable {

  private Integer imgId;
  private Integer targetId;
  private Integer type;
  private String path;
  private Date createDate;


  public Integer getImgId() {
    return imgId;
  }

  public void setImgId(Integer imgId) {
    this.imgId = imgId;
  }


  public Integer getTargetId() {
    return targetId;
  }

  public void setTargetId(Integer targetId) {
    this.targetId = targetId;
  }


  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }


  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

}
