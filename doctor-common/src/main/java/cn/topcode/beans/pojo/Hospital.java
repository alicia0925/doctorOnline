package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Hospital implements Serializable {

  private Integer hId;
  private String hName;
  private String hInfo;
  private String city;
  private String address;
  private Date createDate;
  private Date modifyDate;


  public Integer getHId() {
    return hId;
  }

  public void setHId(Integer hId) {
    this.hId = hId;
  }


  public String getHName() {
    return hName;
  }

  public void setHName(String hName) {
    this.hName = hName;
  }


  public String getHInfo() {
    return hInfo;
  }

  public void setHInfo(String hInfo) {
    this.hInfo = hInfo;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }


  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(Date modifyDate) {
    this.modifyDate = modifyDate;
  }

}
