package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Article implements Serializable {

  private Integer articleId;
  private Integer docId;
  private Integer count;
  private Integer likes;
  private String articleTittle;
  private String summary;
  private String content;
  private Integer tagFirst;
  private Integer tagSecond;
  private Integer tagThree;
  private Date createDate;
  private Date modifyDate;


  public Integer getArticleId() {
    return articleId;
  }

  public void setArticleId(Integer articleId) {
    this.articleId = articleId;
  }


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }


  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }


  public Integer getLikes() {
    return likes;
  }

  public void setLikes(Integer likes) {
    this.likes = likes;
  }


  public String getArticleTittle() {
    return articleTittle;
  }

  public void setArticleTittle(String articleTittle) {
    this.articleTittle = articleTittle;
  }


  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  public Integer getTagFirst() {
    return tagFirst;
  }

  public void setTagFirst(Integer tagFirst) {
    this.tagFirst = tagFirst;
  }


  public Integer getTagSecond() {
    return tagSecond;
  }

  public void setTagSecond(Integer tagSecond) {
    this.tagSecond = tagSecond;
  }


  public Integer getTagThree() {
    return tagThree;
  }

  public void setTagThree(Integer tagThree) {
    this.tagThree = tagThree;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }


  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(Date modifyDate) {
    this.modifyDate = modifyDate;
  }

}
