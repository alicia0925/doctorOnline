package cn.topcode.beans.pojo;


import java.io.Serializable;
import java.util.Date;

public class Answer implements Serializable {

  private Integer aId;
  private Integer docId;
  private String answer;
  private Integer qId;
  private Date createDate;


  public Integer getAId() {
    return aId;
  }

  public void setAId(Integer aId) {
    this.aId = aId;
  }


  public Integer getDocId() {
    return docId;
  }

  public void setDocId(Integer docId) {
    this.docId = docId;
  }


  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }


  public Integer getQId() {
    return qId;
  }

  public void setQId(Integer qId) {
    this.qId = qId;
  }


  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

}
