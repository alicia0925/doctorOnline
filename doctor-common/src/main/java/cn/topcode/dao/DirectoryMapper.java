package cn.topcode.dao;


import cn.topcode.beans.vo.articleVO.ArticleIndexVO;
import cn.topcode.beans.vo.articleVO.ArticleListSearchVO;
import cn.topcode.beans.vo.articleVO.ArticleListVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface DirectoryMapper {


    /***
     * 查询数据字典中所有文章级别
     * @return
     * @throws Exception
     */
    public List<ArticleIndexVO> selectArticleTag() throws Exception;

   }
