package cn.topcode.dao;

import cn.topcode.beans.pojo.Image;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImageMapper {

    /***
     * 添加图片
     * @param image
     * @return
     * @throws Exception
     */
    int insertImage(Image image) throws Exception;
}
