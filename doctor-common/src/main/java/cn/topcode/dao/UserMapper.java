package cn.topcode.dao;

import cn.topcode.beans.pojo.User;
import cn.topcode.beans.vo.userVO.UserRegisterVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    /***
     * 根据手机号查找用户
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    User selectByPhone(@Param("phone") String phone)throws Exception;


    /***
     * 注册用户
     * @param userRegisterVO 注册参数
     * @return
     * @throws Exception
     */
    int insertUser(UserRegisterVO userRegisterVO) throws Exception;
}
