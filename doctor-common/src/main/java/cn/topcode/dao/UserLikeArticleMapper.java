package cn.topcode.dao;

import cn.topcode.beans.pojo.UserLikeArticle;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface UserLikeArticleMapper {

    /***
     * 根据条件查询用户收藏记录
     * @param map 查询条件
     * @return
     * @throws Exception
     */
    UserLikeArticle selectUserLikeArticleByQuery(Map map)throws Exception;

    /***
     * 添加用户收藏记录
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    int insertUserLikeArticle(UserLikeArticle userLikeArticle)throws Exception;

    /***
     * 修改收藏状态
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    int updateUserLikeArticleByQuery(UserLikeArticle userLikeArticle) throws Exception;

}
