package cn.topcode.dao;

import cn.topcode.beans.pojo.Question;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuestionMapper {

    /***
     * 增加提问记录
     * @param question 提问对象
     * @return
     * @throws Exception
     */
    int insertQuestion(Question question) throws Exception;
}
