package cn.topcode.dao;

import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.DoctorSeacherVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DoctorMapper {

    /***
     * 查医生列表
     */

    List<Doctor> getList(DoctorSeacherVo doctorSeacherVo) throws Exception;
}
