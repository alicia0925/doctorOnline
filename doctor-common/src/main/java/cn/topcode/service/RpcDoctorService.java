package cn.topcode.service;

import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.DoctorSeacherVo;

import java.util.List;

public interface RpcDoctorService {

    List<Doctor> findDoctorsByConditions(DoctorSeacherVo doctorSeacherVo)throws Exception;
}
