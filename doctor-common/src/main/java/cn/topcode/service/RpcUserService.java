package cn.topcode.service;

import cn.topcode.beans.vo.userVO.UserRegisterVO;

import cn.topcode.beans.pojo.User;

public interface RpcUserService {

    /***
     * 验证用户是否存在
     * @param phone 手机号
     * @return
     * @throws Exception
     */
    boolean validateUser(String phone) throws Exception;

    /***
     * 注册用户
     * @param userRegisterVO 注册参数
     * @return
     * @throws Exception
     */
    boolean registerUser(UserRegisterVO userRegisterVO)throws Exception;
    /***
     * 登录
     * @param phone 手机号
     * @param password 密码
     * @return
     * @throws Exception
     */
   User login(String phone,String password)throws Exception;

}
