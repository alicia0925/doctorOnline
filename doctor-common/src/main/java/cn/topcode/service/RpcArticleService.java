package cn.topcode.service;

import cn.topcode.beans.vo.articleVO.ArticleListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RpcArticleService {

    /***
     * 根据条件查询所有文章
     * @param map 查询条件
     * @return
     * @throws Exception
     */
    public List<ArticleListVO> findArticleByQuery(Map map) throws Exception;

    /***
     * 修改阅读量
     * @param articleId 文章id
     * @param count 阅读量
     * @return
     * @throws Exception
     */
    public boolean updateCount(Integer articleId, Integer count) throws Exception;

    /***
     * 修改收藏量
     * @param articleId 文章id
     * @param likes 收藏量
     * @return
     * @throws Exception
     */
    public boolean updateLikes(Integer articleId, Integer likes) throws Exception;


}
