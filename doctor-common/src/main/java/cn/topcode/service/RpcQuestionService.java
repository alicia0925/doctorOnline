package cn.topcode.service;

import cn.topcode.beans.pojo.Question;
import cn.topcode.beans.pojo.User;
import cn.topcode.beans.vo.questionVO.FreeQuestionVO;

public interface RpcQuestionService {
    /***
     * 增加提问记录
     * @param freeQuestionVO
     * @return
     * @throws Exception
     */
    boolean addQuestion(FreeQuestionVO freeQuestionVO, User currentUser) throws Exception;
}
