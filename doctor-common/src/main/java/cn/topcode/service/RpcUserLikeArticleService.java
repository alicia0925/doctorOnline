package cn.topcode.service;

import cn.topcode.beans.pojo.UserLikeArticle;

import java.util.Map;

public interface RpcUserLikeArticleService {
    /***
     * 根据条件查询用户收藏记录
     * @param map 查询条件
     * @return
     * @throws Exception
     */
    UserLikeArticle findUserLikeArticleByQuery(Map map) throws Exception;

    /***
     * 添加用户收藏记录
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    boolean addUserLikeArticle(UserLikeArticle userLikeArticle) throws Exception;

    /***
     * 修改收藏状态
     * @param userLikeArticle
     * @return
     * @throws Exception
     */
    boolean updateUserLikeArticle(UserLikeArticle userLikeArticle) throws Exception;
}
