package cn.topcode.service;

import cn.topcode.beans.vo.articleVO.ArticleIndexVO;
import cn.topcode.beans.vo.articleVO.ArticleListSearchVO;
import cn.topcode.beans.vo.articleVO.ArticleListVO;

import java.util.List;

/***
 * 字典服务相关接口
 */
public interface RpcDirectoryService {

    /***
     * 查询数据字典中所有文章级别
     * @return
     * @throws Exception
     */
     List<ArticleIndexVO> findArticleTag() throws Exception;

    }
