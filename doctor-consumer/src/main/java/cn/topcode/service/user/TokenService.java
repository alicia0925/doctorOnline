package cn.topcode.service.user;

import cn.topcode.beans.pojo.User;

public interface TokenService {
    /**
     * 生成token
     */
    String generateToken(String userAgent, User user) throws Exception;


    /**
     * 保存token
     */
    void saveToken(String token, User user) throws Exception;

    /**
     * 验证Token
     */
    boolean validate(String userAgent, String token) throws Exception;

    /**
     * 从redis里获取用户
     *
     * @param token
     * @return
     */
    User load(String token) throws Exception;

    /**
     * 删除token
     *
     * @param token
     */
    void delete(String token) throws Exception;

    /**
     * 置换token
     *
     * @param agent
     * @param token
     * @return
     * @throws Exception
     */
    String replaceToken(String agent, String token) throws Exception;
}
