package cn.topcode.service.user;

import cn.topcode.beans.pojo.User;

public interface UserService {

    User login(String phone,String password)throws Exception;
}
