package cn.topcode.service.user;

import cn.topcode.beans.pojo.User;
import cn.topcode.service.RpcTokenService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {
    @Reference
    private RpcTokenService rpcTokenService;

    @Override
    public String generateToken(String userAgent, User user) throws Exception {
        return rpcTokenService.generateToken(userAgent, user);
    }

    @Override
    public void saveToken(String token, User user) throws Exception {
        rpcTokenService.saveToken(token, user);

    }

    @Override
    public boolean validate(String userAgent, String token) throws Exception {
        return rpcTokenService.validate(userAgent, token);
    }

    @Override
    public User load(String token) throws Exception {
        return rpcTokenService.load(token);
    }

    @Override
    public void delete(String token) throws Exception {
        rpcTokenService.delete(token);
    }

    @Override
    public String replaceToken(String agent, String token) throws Exception {
        return rpcTokenService.replaceToken(agent,token);
    }
}
