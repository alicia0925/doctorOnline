package cn.topcode.service.user;

import cn.topcode.beans.pojo.User;
import cn.topcode.service.RpcUserService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    @Reference
    private RpcUserService rpcUserService;


    @Override
    public User login(String phone, String password) throws Exception {
        return rpcUserService.login(phone,password);
    }
}
