package cn.topcode.service.solrdoc;

import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.SolrDoctorVO;

import java.util.List;

public interface SolrService {

    List<Doctor>  getDocList(SolrDoctorVO solrDoctorVO) throws Exception;
}
