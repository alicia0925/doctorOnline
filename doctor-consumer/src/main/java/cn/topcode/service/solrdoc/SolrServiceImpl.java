package cn.topcode.service.solrdoc;

import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.SolrDoctorVO;
import cn.topcode.utils.EmptyUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SolrServiceImpl implements SolrService {
    @Resource
    private SolrClient solrClient;
//    private HttpSolrClient solrClient;

    @Override
    public List<Doctor> getDocList(SolrDoctorVO solrDoctorVO) throws Exception {
//        ModifiableSolrParams params = new ModifiableSolrParams();
//        params.add("q", "docId:1");
        //查询条件封装到语句中
        SolrQuery query = new SolrQuery("*:*");

        List<Doctor> doctors = new ArrayList<>();

        if (EmptyUtils.isEmpty(solrDoctorVO)) {

            QueryResponse queryResponse = solrClient.query(query);
            SolrDocumentList results = queryResponse.getResults();
            doctors = queryResponse.getBeans(Doctor.class);
        }

        return doctors;
    }
}
