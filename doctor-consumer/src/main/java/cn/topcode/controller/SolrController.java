package cn.topcode.controller;

import cn.topcode.beans.dto.Dto;
import cn.topcode.beans.pojo.Doctor;
import cn.topcode.beans.vo.doctor.SolrDoctorVO;
import cn.topcode.utils.DtoUtil;
import cn.topcode.service.solrdoc.SolrService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/solr")
public class SolrController {

    @Resource
    private SolrService solrService;

    @RequestMapping(value = "/getlist",method = RequestMethod.GET)

    public Dto getList(@RequestParam String depId,
                       @RequestParam String descSort,
                       @RequestParam String ascSort){
        SolrDoctorVO solrDoctorVo =null;
        List<Doctor> doctors=null;
        try {
            doctors= solrService.getDocList(solrDoctorVo);
            return DtoUtil.returnDataSuccess(doctors);
        } catch (Exception e) {
            e.printStackTrace();
            return DtoUtil.returnFail("异常","1212121");
        }

    }


}