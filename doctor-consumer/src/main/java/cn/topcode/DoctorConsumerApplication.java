package cn.topcode;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;

@SpringBootApplication(exclude = {GsonAutoConfiguration.class})
@EnableDubboConfiguration
public class DoctorConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoctorConsumerApplication.class, args);
    }
}
